#!/bin/bash

find . -name "*.md*" | while read i; do pandoc -f markdown -t latex "$i" -o "${i%.*}.pdf"; done
