SHELL := /bin/bash

.PHONY: convert

convert:
	source ./md-to-docx.sh
	source ./md-to-pdf.sh

dist: convert
	zip -r docs.zip *.pdf *.docx
