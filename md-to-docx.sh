#!/bin/bash

find . -name "*.md*" | while read i; do pandoc -f markdown -t docx "$i" -o "${i%.*}.docx"; done
