## Kollaboratives Schreiben

Um die Dokumentation effizienter zu gestalten, laufen CI/CD-Prozesse, die **kontinuierlich** und **rekursiv** alle `.md`-Dateien in diesem Repository mithilfe von [Pandoc](https://pandoc.org/) in `.pdf`- und `.docx`-Dateien konvertieren.

[![][badge]][download-artifact]

[download-artifact]: https://gitlab.com/diegosiqueir4/computer-cafe/-/jobs/artifacts/main/raw/docs.zip?job=Docs+convertion
[badge]: https://img.shields.io/badge/Download-.ZIP-green.svg?style=for-the-badge&logo=docusign

### Computer Café

## Tools zur Kollaboration

|  | |  |
| -------- | -------- | -------- |
| [Gitlab](https://gitlab.com/)     | [Etherpad](https://cryptpad.fr/)    | [Hedgedoc](https://hedgedoc.org/)     |
| [GoogleDocs](https://docs.google.com/)    | [Slack](https://slack.com/intl/de-de)    | [Mattermost](https://mattermost.com/)     |
|  [Wikis / Intranet](https://www.mediawiki.org/wiki/MediaWiki/de)   | [Dontpad](https://dontpad.com/)    | [HackMD](hackmd.io/)    |
|  [Conceptboard](https://conceptboard.com/)   | [Overleaf](https://www.overleaf.com/)    | [Miro](https://miro.com/de/)    |


## Einsatzszenarien (1/2)

- Gitlab: **Kollaboratives Projektmanagement**
- Etherpad, Hedgedoc, Googledocs, Overleaf: **Kollaboratives Verwalten von Dokumenten**
- Slack, Mattermost: **Schnelle, unkomplizierte Kommunikation (Chat, Dateien teilen)**

## Einsatzszenarien (2/2)

- Dontpad: **Schnelle Vermittlung/Schnelles Teilen von Links**
- Wikis: **Erstellung gemeinsamer Wikis/Intranet für Workflows, usw.**
- Miro, Conceptboard: **Gemeinsame Erstellung von Texten und Präsentationen**
