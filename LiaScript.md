##  Insider-tip

[LiaScript](https://github.com/LiaScript/LiaScript) ist eine Erweiterung von Markdown zur Unterstützung der Erstellung von kostenlosen und offenen Online-Kursen, interaktiven Büchern und somit von Open Educational Resources (OER). Kurse sind einfache Textdateien, die von jedermann frei gehostet und erstellt werden können, ähnlich wie Open-Source-Projekte.
